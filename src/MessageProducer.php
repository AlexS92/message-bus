<?php
declare(strict_types=1);

namespace MessageBus;

use MessageBus\DomainMessage\PayloadMessageInterface;
use MessageBus\MessageProducerEvent\MessageProducerPostPublishEvent;
use OpenTracing\Formats;
use OpenTracing\NoopTracer;
use OpenTracing\Scope;
use OpenTracing\Tags;
use OpenTracing\Tracer;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Exception\AMQPRuntimeException;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Ramsey\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\SerializerInterface;

class MessageProducer
{
    private AbstractConnection $connection;
    private SerializerInterface $serializer;
    private EventDispatcherInterface $eventDispatcher;
    private string $exchangeName;
    private LoggerInterface $logger;
    private Tracer $tracer;

    public function __construct(
        AbstractConnection $connection,
        SerializerInterface $serializer,
        EventDispatcherInterface $eventDispatcher,
        string $exchangeName,
        LoggerInterface $logger = null,
        Tracer $tracer = null
    ) {
        $this->connection = $connection;
        $this->serializer = $serializer;
        $this->exchangeName = $exchangeName;
        $this->eventDispatcher = $eventDispatcher;
        $this->tracer = $tracer ?? new NoopTracer();
        $this->logger = $logger ?? new NullLogger();
    }

    /** @throws \Throwable */
    public function publish(PayloadMessageInterface $payloadMessage): void
    {
        $uuid = Uuid::uuid4()->toString();
        $eventName = $payloadMessage->getEventName();

        $message = $this->createAMQPMessage($uuid, $payloadMessage);

        $scope = $this->tracer->startActiveSpan('publish domain event');
        $this->injectTracerHeaderInMassage($scope, $message, $uuid, $eventName);

        try {
            $this->publishMessageByRoutingKey($message, $eventName);

            $event = new MessageProducerPostPublishEvent($eventName);
            $this->eventDispatcher->dispatch($event, MessageProducerPostPublishEvent::NAME);
        } finally {
            $scope->close();
        }
    }

    /** @throws \Exception */
    private function publishMessageByRoutingKey(AMQPMessage $message, string $routingKey): void
    {
        try {
            $channel = $this->connection->channel();
            $channel->exchange_declare($this->exchangeName, 'topic', false, true, false, false, false);
            $channel->basic_publish($message, $this->exchangeName, $routingKey);
        } catch (AMQPRuntimeException $exception) {
            $this->logger->error($exception->getMessage(), $exception->getTrace());
            $this->connection->reconnect();
            $this->publishMessageByRoutingKey($message, $routingKey);
        }
    }

    /** @throws \Throwable */
    private function createAMQPMessage(string $uuid, PayloadMessageInterface $event): AMQPMessage
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1)[0];

        $metadata = [
            'uuid'      => $uuid,
            'timestamp' => time(),
            'source'    => "{$trace['file']}:{$trace['line']}",
            'hostname'  => gethostname() ?: 'unknown',
        ];

        $data = [
            'event'    => $event->getEventName(),
            'metadata' => $metadata,
            'payload'  => $event,
        ];

        try {
            $jsonMessage = $this->serializer->serialize($data, 'json');
        } catch (\Throwable $exception) {
            $this->logger->error($exception->getMessage(), $exception->getTrace());
            throw $exception;
        }

        return new AMQPMessage($jsonMessage);
    }

    private function injectTracerHeaderInMassage(Scope $scope, AMQPMessage $message, string $uuid, string $eventName): void
    {
        $span = $scope->getSpan();
        $span->setTag(Tags\SPAN_KIND, Tags\SPAN_KIND_MESSAGE_BUS_PRODUCER);
        $span->setTag('event_id', $uuid);
        $span->setTag('event_name', $eventName);

        try {
            $headers = [];

            $this->tracer->inject($span->getContext(), Formats\TEXT_MAP, $headers);

            $message->set('application_headers', new AMQPTable($headers));
        } catch (\Throwable $exception) {
            $this->logger->warning("Can't inject header in message with name", [
                'eventName' => $eventName,
            ]);
        }
    }
}
