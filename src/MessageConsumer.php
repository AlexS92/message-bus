<?php
declare(strict_types=1);

namespace MessageBus;

use MessageBus\DomainMessage\Message;
use MessageBus\DomainMessage\PayloadMessageInterface;
use MessageBus\MessageConsumerEvent\MessageConsumerPostExecuteEvent;
use MessageBus\MessageConsumerEvent\MessageConsumerPreExecuteEvent;
use OpenTracing\Formats;
use OpenTracing\NoopTracer;
use OpenTracing\Reference;
use OpenTracing\Scope;
use OpenTracing\Tags;
use OpenTracing\Tracer;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @phpstan-type ExecutorMapByEventName array<string, array{"class_serialize": string, "executor_list": string[]}>
 */
class MessageConsumer
{
    private LoggerInterface $logger;
    private ContainerInterface $container;
    private EventDispatcherInterface $eventDispatcher;
    private Tracer $tracer;
    private ValidatorInterface $validator;

    /** @var SerializerInterface & DenormalizerInterface */
    private SerializerInterface $serializer;

    /** @var ExecutorMapByEventName */
    private array $messageExecutorMap;

    /**
     * @param SerializerInterface & DenormalizerInterface $serializer
     * @param ExecutorMapByEventName $messageExecutorMap
     */
    public function __construct(
        SerializerInterface $serializer,
        ContainerInterface $container,
        EventDispatcherInterface $eventDispatcher,
        ValidatorInterface $validator,
        array $messageExecutorMap,
        LoggerInterface $logger = null,
        Tracer $tracer = null
    ) {
        $this->serializer = $serializer;
        $this->container = $container;
        $this->eventDispatcher = $eventDispatcher;
        $this->validator = $validator;
        $this->messageExecutorMap = $messageExecutorMap;
        $this->logger = $logger ?? new NullLogger();
        $this->tracer = $tracer ?? new NoopTracer();
    }

    /** @param ExecutorMapByEventName $messageExecutorMap */
    public function setMessageExecutorMap(array $messageExecutorMap): void
    {
        $this->messageExecutorMap = $messageExecutorMap;
    }

    /** @throws \Throwable */
    public function execute(AMQPMessage $message): void
    {
        $this->enableGarbageCollector();

        $scope = $this->getTraceScope($message);
        $span = $scope->getSpan();
        $span->setTag(Tags\COMPONENT, 'php-message-bus');
        $span->setTag(Tags\SPAN_KIND, Tags\SPAN_KIND_MESSAGE_BUS_CONSUMER);

        try {
            /** @var Message $domainEventMessage */
            $domainEventMessage = $this->serializer->deserialize($message->getBody(), Message::class, 'json');

            $domainEventName = $domainEventMessage->getEventName();
            $executorMap = $this->messageExecutorMap[$domainEventName] ?? null;
            if (null === $executorMap) {
                throw new \Exception("Undefined execution classmap for event: {$domainEventName}.");
            }

            $metaData = $domainEventMessage->getMetadata();
            $payload = $this->serializer->denormalize($domainEventMessage->getPayload(), $executorMap['class_serialize']);

            $violationList = $this->validator->validate($payload);
            if ($violationList->count() > 0) {
                //todo throw validation exception
            }

            $event = new MessageConsumerPreExecuteEvent($domainEventName);
            $this->eventDispatcher->dispatch($event, MessageConsumerPreExecuteEvent::NAME);

            $executorList = $this->getExecutorList(...$executorMap['executor_list']);
            foreach ($executorList as $executor) {
                $executor->execute($payload, $metaData);
            }

            $event = new MessageConsumerPostExecuteEvent($domainEventName);
            $this->eventDispatcher->dispatch($event, MessageConsumerPostExecuteEvent::NAME);
        } catch (\Throwable $exception) {
            $span->setTag(Tags\ERROR, true);
            $this->logger->critical($exception->getMessage(), $exception->getTrace());

            throw $exception;
        } finally {
            $scope->close();
            $this->tracer->flush();

            gc_collect_cycles();
        }
    }

    private function enableGarbageCollector(): void
    {
        if (!gc_enabled()) {
            gc_enable();
        }
    }

    /**
     * @return MessageExecutorInterface<PayloadMessageInterface>[]
     *
     * @throws ContainerExceptionInterface | NotFoundExceptionInterface
     */
    private function getExecutorList(string ...$executorServiceNameList): array
    {
        $executorList = [];
        foreach ($executorServiceNameList as $executorServiceName) {
            $executorList[] = $this->container->get($executorServiceName);
        }

        return $executorList;
    }

    private function getTraceScope(AMQPMessage $message): Scope
    {
        $options = [];
        try {
            /** @var AMQPTable $headers */
            $headers = $message->get('application_headers');
            $spanContext = $this->tracer->extract(Formats\TEXT_MAP, $headers->getNativeData());
            if ($spanContext !== null) {
                $options = [
                    'references' => [ new Reference(Reference::FOLLOWS_FROM, $spanContext) ],
                ];
            }
        } catch (\Throwable $exception) {
            $this->logger->debug(
                'Message without trace',
                ['message' => $message->getBody()]
            );
        }

        return $this->tracer->startActiveSpan('handle amqp message', $options);
    }
}
