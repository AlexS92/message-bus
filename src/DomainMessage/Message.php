<?php
declare(strict_types=1);

namespace MessageBus\DomainMessage;

class Message
{
    private string $eventName;
    private MessageMetadata $metadata;

    /** @var array<string, mixed> */
    private array $payload;

    /** @param array<string, mixed> $payload */
    public function __construct(string $event, MessageMetadata $metadata, array $payload)
    {
        $this->eventName = $event;
        $this->metadata = $metadata;
        $this->payload = $payload;
    }

    public function getEventName(): string
    {
        return $this->eventName;
    }

    public function getMetadata(): MessageMetadata
    {
        return $this->metadata;
    }

    /** @return array<string, mixed> */
    public function getPayload(): array
    {
        return $this->payload;
    }
}
