<?php
declare(strict_types=1);

namespace MessageBus\DomainMessage;

interface PayloadMessageInterface
{
    public function getEventName(): string;
}
