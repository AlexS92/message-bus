<?php
declare(strict_types=1);

namespace MessageBus\DomainMessage;

class MessageMetadata
{
    private string $uuid;
    private int $timestamp;
    private string $source;
    private string $hostname;

    public function __construct(string $uuid, int $timestamp, string $source, string $hostname)
    {
        $this->uuid = $uuid;
        $this->timestamp = $timestamp;
        $this->source = $source;
        $this->hostname = $hostname;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getHostname(): string
    {
        return $this->hostname;
    }
}
