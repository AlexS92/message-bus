<?php
declare(strict_types=1);

namespace MessageBus\DomainMessage\Payload\User\UserVerification;

use MessageBus\DomainMessage\PayloadMessageInterface;

class UserVerificationRequestCreatedMessagePayload implements PayloadMessageInterface
{
    public const NAME = 'UserVerificationRequestCreated';

    private int $userId;
    private int $requestId;
    private int $timestamp;

    public function __construct(int $userId, int $requestId, int $timestamp)
    {
        $this->userId = $userId;
        $this->requestId = $requestId;
        $this->timestamp = $timestamp;
    }

    public function getEventName(): string
    {
        return self::NAME;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getRequestId(): int
    {
        return $this->requestId;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }
}
