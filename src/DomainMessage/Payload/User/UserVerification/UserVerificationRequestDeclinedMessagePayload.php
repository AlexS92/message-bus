<?php
declare(strict_types=1);

namespace MessageBus\DomainMessage\Payload\User\UserVerification;

use MessageBus\DomainMessage\PayloadMessageInterface;

class UserVerificationRequestDeclinedMessagePayload implements PayloadMessageInterface
{
    public const NAME = 'UserVerificationRequestDeclined';

    private int $userId;
    private int $requestId;
    private string $response;
    private int $timestamp;

    public function __construct(int $userId, int $requestId, string $response, int $timestamp)
    {
        $this->userId = $userId;
        $this->requestId = $requestId;
        $this->response = $response;
        $this->timestamp = $timestamp;
    }

    public function getEventName(): string
    {
        return self::NAME;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getRequestId(): int
    {
        return $this->requestId;
    }

    public function getResponse(): string
    {
        return $this->response;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }
}
