<?php
declare(strict_types=1);

namespace MessageBus\DomainMessage\Payload\User;

use MessageBus\DomainMessage\PayloadMessageInterface;

class UserCreatedMessagePayload implements PayloadMessageInterface
{
    public const NAME = 'UserCreated';

    private int $userId;
    private string $email;
    private string $firstName;
    private string $lastName;
    private string $emailConfirmationCode;
    private int $timestamp;

    public function __construct(int $userId, string $email, string $firstName, string $lastName, string $emailConfirmCode, int $timestamp)
    {
        $this->userId                = $userId;
        $this->email                 = $email;
        $this->firstName             = $firstName;
        $this->lastName              = $lastName;
        $this->timestamp             = $timestamp;
        $this->emailConfirmationCode = $emailConfirmCode;
    }

    public function getEventName(): string
    {
        return self::NAME;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmailConfirmationCode(): string
    {
        return $this->emailConfirmationCode;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }
}
