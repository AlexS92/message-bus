<?php
declare(strict_types=1);

namespace MessageBus\MessageConsumerEvent;

class MessageConsumerPreExecuteEvent
{
    public const NAME = 'message_consumer.pre_execute_event';

    private string $domainEventName;

    public function __construct(string $domainEventName)
    {
        $this->domainEventName = $domainEventName;
    }

    public function getDomainEventName(): string
    {
        return $this->domainEventName;
    }
}
