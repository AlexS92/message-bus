<?php
declare(strict_types=1);

namespace MessageBus;

use MessageBus\DomainMessage\MessageMetadata;
use MessageBus\DomainMessage\PayloadMessageInterface;

/**
 * @template T of PayloadMessageInterface
 * @method void execute( T $payload, ?MessageMetadata $metaData = null)
 */
interface MessageExecutorInterface
{
    //Marker
}
