<?php
declare(strict_types=1);

namespace MessageBus\MessageProducerEvent;

class MessageProducerPostPublishEvent
{
    public const NAME = 'message_producer.post_publish_event';

    private string $domainEventName;

    public function __construct(string $domainEventName)
    {
        $this->domainEventName = $domainEventName;
    }

    public function getDomainEventName(): string
    {
        return $this->domainEventName;
    }
}
