<?php
declare(strict_types=1);

namespace MessageBus\Tests;

use _PHPStan_4dd92cd93\Nette\Neon\Exception;
use MessageBus\MessageProducer;
use MessageBus\MessageProducerEvent\MessageProducerPostPublishEvent;
use MessageBus\Tests\Fixtures\DummyMessagePayload;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\SerializerInterface;

require_once __DIR__ . '/DummyCoreFunction.php';

class MessageProducerTest extends TestCase
{
    private const MESSAGE_PAYLOAD_ID = 5;

    private string $exchangeName;

    /** @var AbstractConnection & MockObject */
    private AbstractConnection $connection;

    /** @var SerializerInterface & MockObject */
    private SerializerInterface $serializer;

    /** @var EventDispatcherInterface & MockObject */
    private EventDispatcherInterface $eventDispatcher;

    protected function setUp(): void
    {
        global $mockDebugTrace, $mockRandomBytes, $mockTime;
        $mockDebugTrace = true;
        $mockRandomBytes = true;
        $mockTime = true;

        $this->exchangeName = 'exchange';
        $this->connection = $this->createMock(AbstractConnection::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
    }

    /** @test */
    public function publish_PayloadMessage_void(): void
    {
        $messagePayload = new DummyMessagePayload(self::MESSAGE_PAYLOAD_ID);
        $this->whenEventDispatcherDispatchVoidReturned($messagePayload);
        $jsonString = $this->whenSerializerSerializePayloadJsonStringReturned($messagePayload);
        $message = new AMQPMessage($jsonString);
        $message->set('application_headers', new AMQPTable([]));
        $channelMock = $this->whenConnectionGetChannelChannelReturned();
        $this->channelExpectsExchangeDeclareOnce($channelMock);
        $this->channelExpectsBasicPublishWithAMQPMessage($channelMock, $message, $messagePayload);
        $messageProducer = $this->createMessageProducer();

        $messageProducer->publish($messagePayload);
    }

    private function createMessageProducer(): MessageProducer
    {
        return new MessageProducer(
            $this->connection,
            $this->serializer,
            $this->eventDispatcher,
            $this->exchangeName
        );
    }

    private function channelExpectsBasicPublishWithAMQPMessage(MockObject $channelMock, AMQPMessage $message, DummyMessagePayload $messagePayload): void
    {
        $channelMock
            ->expects($this->once())
            ->method('basic_publish')
            ->with($message, $this->exchangeName, $messagePayload->getEventName(), false, false, null)
        ;
    }

    private function whenEventDispatcherDispatchVoidReturned(DummyMessagePayload $messagePayload): void
    {
        $this->eventDispatcher
            ->expects($this->once())
            ->method('dispatch')
            ->with(
                new MessageProducerPostPublishEvent(
                    $messagePayload->getEventName()
                )
            )
        ;
    }

    private function channelExpectsExchangeDeclareOnce(MockObject $channelMock): void
    {
        $channelMock
            ->expects($this->once())
            ->method('exchange_declare')
            ->with($this->exchangeName, 'topic', false, true, false, false, false, [], null)
        ;
    }

    private function whenConnectionGetChannelChannelReturned(): MockObject
    {
        $channelMock = $this->createMock(AMQPChannel::class);

        $this->connection
            ->expects($this->once())
            ->method('channel')
            ->willReturn($channelMock)
        ;

        return $channelMock;
    }

    private function whenSerializerSerializePayloadJsonStringReturned(DummyMessagePayload $dummyMessagePayload): string
    {
        $data = $this->createPayloadMap($dummyMessagePayload);
        $jsonString = json_encode($data);

        if (false === $jsonString) {
            throw new Exception('Wrong JSON in the test');
        }

        $this->serializer
            ->expects($this->once())
            ->method('serialize')
            ->with($data, 'json', [])
            ->willReturn($jsonString)
        ;

        return $jsonString;
    }

    /** @return array<string, mixed> */
    private function createPayloadMap(DummyMessagePayload $dummyMessagePayload): array
    {
        $uuid = '736f6d65-736f-4d65-b36f-6d65';

        $metadata = [
            'uuid'      => $uuid,
            'timestamp' => 1,
            'source'    => 'file:line',
            'hostname'  => gethostname() ?: 'unknown',
        ];

        return [
            'event'    => $dummyMessagePayload->getEventName(),
            'metadata' => $metadata,
            'payload'  => $dummyMessagePayload,
        ];
    }
}
