<?php
declare(strict_types=1);

namespace MessageBus\Tests\Fixtures;

use MessageBus\DomainMessage\MessageMetadata;
use MessageBus\MessageExecutorInterface;

/** @implements MessageExecutorInterface<DummyMessagePayload> */
class DummyExecutor implements MessageExecutorInterface
{
    public function execute(DummyUserCreateMessage $payload, ?MessageMetadata $metaData = null): void
    {
    }
}
