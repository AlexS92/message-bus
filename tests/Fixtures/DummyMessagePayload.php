<?php
declare(strict_types=1);

namespace MessageBus\Tests\Fixtures;

use MessageBus\DomainMessage\PayloadMessageInterface;

class DummyMessagePayload implements PayloadMessageInterface
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getEventName(): string
    {
        return 'DummyMessagePayload';
    }

    public function getId(): int
    {
        return $this->id;
    }
}
