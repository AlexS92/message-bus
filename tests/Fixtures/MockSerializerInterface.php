<?php

namespace MessageBus\Tests\Fixtures;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/** For PHPUnit > 8.5 */
interface MockSerializerInterface extends SerializerInterface, DenormalizerInterface
{
}
