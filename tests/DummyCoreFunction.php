<?php
declare(strict_types=1);

namespace {
    $mockDebugTrace = false;
    $mockRandomBytes = false;
    $mockTime = false;
}

namespace MessageBus {
    function time(): int
    {
        global $mockTime;
        if (isset($mockTime) && true === $mockTime) {
            return 1;
        }

        return \time();
    }

    /** @return array<string|int, mixed> */
    function debug_backtrace(): array
    {
        global $mockDebugTrace;

        if (isset($mockDebugTrace) && true === $mockDebugTrace) {
            return [
                [
                    'file' => 'file',
                    'line' => 'line',
                ],
            ];
        }

        return \debug_backtrace(...func_get_args());
    }
}

namespace Ramsey\Uuid\Generator {

    use Ramsey\Uuid\Uuid;

    /**
     * @see Uuid::uuid4()
     *
     * @throws \Throwable
     *
     * string value 736f6d65-736f-4d65-b36f-6d65
     */
    function random_bytes(): string
    {
        global $mockRandomBytes;

        if (isset($mockRandomBytes) && true === $mockRandomBytes) {
            return 'somesomesome';
        }

        return \random_bytes(...func_get_args());
    }
}
