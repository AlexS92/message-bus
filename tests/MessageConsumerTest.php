<?php
declare(strict_types=1);

namespace MessageBus\Tests;

use _PHPStan_4dd92cd93\Nette\Neon\Exception;
use MessageBus\DomainMessage\Message;
use MessageBus\DomainMessage\MessageMetadata;
use MessageBus\MessageConsumer;
use MessageBus\MessageConsumerEvent\MessageConsumerPostExecuteEvent;
use MessageBus\MessageConsumerEvent\MessageConsumerPreExecuteEvent;
use MessageBus\Tests\Fixtures\DummyExecutor;
use MessageBus\Tests\Fixtures\DummyUserCreateMessage;
use MessageBus\Tests\Fixtures\MockSerializerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/** @phpstan-import-type ExecutorMapByEventName from MessageConsumer */
class MessageConsumerTest extends TestCase
{
    public const EVENT_NAME = 'UserCreated';

    public const MESSAGE = [
        'event'    => self::EVENT_NAME,
        'metadata' =>
        [
            'uuid'      => '3d134e53-93dd-44bc-ba9b-cc0ceb6e9e44',
            'timestamp' => 1592838164,
            'source'    => 'DomainEventSender.php:98',
            'hostname'  => 'fpm',
        ],
        'payload'  =>
        [
            'userId'    => 9,
            'email'     => 'some6@mail.com',
            'firstName' => 'firstName',
            'lastName'  => 'lastName',
        ],
    ];

    public const EXECUTOR_LIST = [
        'first_executor',
        'second_executor',
    ];

    /** @var LoggerInterface & MockObject */
    private LoggerInterface $logger;

    /** @var MockSerializerInterface & MockObject */
    private SerializerInterface $serializer;

    /** @var ContainerInterface & MockObject */
    private ContainerInterface $container;

    /** @var ValidatorInterface & MockObject */
    private ValidatorInterface $validator;

    /** @var EventDispatcherInterface & MockObject */
    private EventDispatcherInterface $eventDispatcher;

    /** @var ExecutorMapByEventName */
    private array $messageExecutorMap;

    protected function setUp(): void
    {
        $this->messageExecutorMap = $this->createMapExecutor();
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->container = $this->createMock(ContainerInterface::class);
        $this->serializer = $this->createMock(MockSerializerInterface::class);
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
    }

    /** @test */
    public function execute_AMQPMessage_void(): void
    {
        $metadata = $this->createMessageMetadata();
        $this->whenSerializerSerializeMessageMessageReturned($metadata);
        $this->whenEventDispatcherDispatchVoidReturned();
        $this->whenValidatorValidateEmptyViolationListReturned();
        $payload = $this->whenSerializerDenormalizePayloadPayloadReturned();
        $executorList = $this->whenContainerGetExecutorsExecutorListReturned();
        $this->expectExecutorsCallExecuteMethod($payload, $metadata, ...$executorList);
        $AMQPMessage = $this->createAMQPMessage();
        $messageConsumer = $this->createMessageConsumer();

        $messageConsumer->execute($AMQPMessage);
    }

    /** @test */
    public function execute_with_nullExecutionMap_exceptionExpected(): void
    {
        $this->messageExecutorMap = [];
        $metadata = $this->createMessageMetadata();
        $this->whenSerializerSerializeMessageMessageReturned($metadata);
        $AMQPMessage = $this->createAMQPMessage();
        $messageConsumer = $this->createMessageConsumer();
        $this->expectException('Exception');
        $this->expectExceptionMessage('Undefined execution classmap for event: ' . self::EVENT_NAME . '.');

        $this->logger
            ->expects(self::once())
            ->method('critical')
            ->withAnyParameters()
        ;

        $messageConsumer->execute($AMQPMessage);
    }

    private function expectExecutorsCallExecuteMethod(DummyUserCreateMessage $payload, MessageMetadata $metadata, MockObject ...$executorList): void
    {
        foreach ($executorList as $executor) {
            $executor
                ->expects(self::once())
                ->method('execute')
                ->with($payload, $metadata)
            ;
        }
    }

    private function whenValidatorValidateEmptyViolationListReturned(): void
    {
        $this->validator
            ->expects(self::once())
            ->method('validate')
            ->withAnyParameters()
            ->willReturn(new ConstraintViolationList());
    }

    private function whenSerializerDenormalizePayloadPayloadReturned(): DummyUserCreateMessage
    {
        $payload = $this->createPayload();
        $this->serializer
            ->expects(self::once())
            ->method('denormalize')
            ->with(self::MESSAGE['payload'], DummyUserCreateMessage::class, null, [])
            ->willReturn($payload)
        ;

        return $payload;
    }

    private function createMessageConsumer(): MessageConsumer
    {
        return new MessageConsumer(
            $this->serializer,
            $this->container,
            $this->eventDispatcher,
            $this->validator,
            $this->messageExecutorMap,
            $this->logger
        );
    }

    private function whenEventDispatcherDispatchVoidReturned(): void
    {
        $this->eventDispatcher
            ->expects(self::exactly(2))
            ->method('dispatch')
            ->withConsecutive(
                [new MessageConsumerPreExecuteEvent(self::EVENT_NAME), MessageConsumerPreExecuteEvent::NAME, ],
                [new MessageConsumerPostExecuteEvent(self::EVENT_NAME), MessageConsumerPostExecuteEvent::NAME, ]
            )
        ;
    }

    private function whenSerializerSerializeMessageMessageReturned(MessageMetadata $metadata): void
    {
        $message = $this->createMessage($metadata);
        $decodedString = json_encode(self::MESSAGE);

        $this->serializer
            ->expects(self::once())
            ->method('deserialize')
            ->with(
                $decodedString,
                Message::class,
                'json',
                []
            )
            ->willReturn($message)
        ;
    }

    /** @return ExecutorMapByEventName */
    private function createMapExecutor(): array
    {
        return [
            self::EVENT_NAME => [
                'class_serialize' => DummyUserCreateMessage::class,
                'executor_list'   => self::EXECUTOR_LIST,
            ],
        ];
    }

    /** @throws Exception */
    private function createAMQPMessage(): AMQPMessage
    {
        $decodedString = json_encode(self::MESSAGE);

        if (false === $decodedString) {
            throw new Exception('Wrong JSON in the test');
        }

        return new AMQPMessage($decodedString);
    }

    private function createPayload(): DummyUserCreateMessage
    {
        $payload = self::MESSAGE['payload'];

        return new DummyUserCreateMessage(
            $payload['userId'],
            $payload['email'],
            $payload['firstName'],
            $payload['lastName']
        );
    }

    private function createMessageMetadata(): MessageMetadata
    {
        $metadata = self::MESSAGE['metadata'];

        return new MessageMetadata(
            $metadata['uuid'],
            $metadata['timestamp'],
            $metadata['source'],
            $metadata['hostname']
        );
    }

    private function createMessage(MessageMetadata $metadata): Message
    {
        return new Message(
            self::MESSAGE['event'],
            $metadata,
            self::MESSAGE['payload']
        );
    }

    /** @return MockObject[] */
    private function whenContainerGetExecutorsExecutorListReturned(): array
    {
        $executorList = [];
        foreach (self::EXECUTOR_LIST as $executorId) {
            $executor = $this->createMock(DummyExecutor::class);
            $executorList[] = $executor;
        }

        $this->container
            ->expects(self::exactly(count(self::EXECUTOR_LIST)))
            ->method('get')
            ->withConsecutive(
                [self::EXECUTOR_LIST[0]],
                [self::EXECUTOR_LIST[1]]
            )->willReturnOnConsecutiveCalls(
                $executorList[0],
                $executorList[1]
            );

        return $executorList;
    }
}
