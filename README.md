## OLD VERSION

# Message-Bus
This is a library for collecting and understanding the structure of all domain events sent via RabbitMQ (AMQP Bus) for PHP services. The library includes consumer and producer implementations.

## Consumer
The `MessageExecutorInterface` must be implemented by the executor. Executors must have an `execute` method (you can see the `tests`). There are internal events such as `MessageConsumerPreExecuteEvent` and `MessageConsumerPostExecuteEvent` before and after execution, respectively.

### Sample of `MessageConsumer's` map
```php
[
    'nameOfEvent' => [
        'class_serialize' => 'payLoadClass',
        'executor_list' => [
            'executor_1',
            'executor_2',
            // ...
        ],
    ]
];
```

## Producer

```php
$payload = new UserCreatedMessagePayload(...);
$this->producer->publish($payload);
```

The Producer also have an internal event - `MessageProducerPostPublishEvent`.

# WARNING
Keep in mind that if the consumer is running as a daemon, YOUR code is in charge of database connections.
